const express = require("express");
const fs = require("fs");
const router = express.Router();

router.get("/", (req, res) => {
  fs.readFile("storage.json", (err, json) => {
    if (err) {
      res.status(500).json(err);
    }

    let obj = JSON.parse(json);
    res.status(200).json(obj);
  });
});

router.post("/", (req, res) => {
  const data = req.body;
  //console.log(data);

  fs.readFile("storage.json", (err, json) => {
    if (err) {
      res.status(500).json(err);
    }
    let list = JSON.parse(json);

    list.push(data);

    fs.writeFile("storage.json", JSON.stringify(list), (err, json) => {
      let obj = json;
      res.status(200).json(obj);
    });
  });
});

router.patch("/", (req, res) => {
  const data = req.body;
  console.log(data);

  //take date from input for send push notification
  const pushTime = data.time;
  const convertTime = new Date(pushTime);
  global.timeofNotification = convertTime.valueOf();
  global.titleNotification = data.title;
  global.descriptNotification = data.description;
  global.deviceId = data.registrationId;
  global.todoId = data.id;

  fs.readFile("storage.json", (err, json) => {
    if (err) {
      res.status(500).json(err);
    }

    let list = JSON.parse(json);

    const foundIndex = list.findIndex(x => x.id === data.id);
    list[foundIndex] = data;

    fs.writeFile("storage.json", JSON.stringify(list), (err, json) => {
      let obj = json;
      res.status(200).json(obj);
    });
  });
});

router.delete("/", (req, res) => {
  console.log(req);
  const index = +req.body.index;

  fs.readFile("storage.json", (err, json) => {
    if (err) {
      res.status(500).json(err);
    }

    let list = JSON.parse(json);
    //console.log('before',list.map(({title}) => title));
    list.splice(index, 1);
    //console.log('after',list.map(({title}) => title));

    fs.writeFile("storage.json", JSON.stringify(list), (err, json) => {
      let obj = json;
      res.status(200).json(obj);
    });
  });
});

module.exports = router;
